FROM telegraf

COPY telegraf.conf /etc/telegraf/telegraf.conf

RUN /etc/init.d/telegraf start
